<?php

/**
 * @file
 * Example: Migrates users from Drupal 6 to 7.
 */

/**
 * Migrates users from Drupal 6 to 7 (example class).
 *
 * This example class shows how to make your own class to migrate users from
 * a Drupal 6 to Drupal 7, using the Drupal 6 to 7 Migrate module. You will
 * just need to modify the roleMap and formatMap sections below for use in your
 * own migration.
 */
class ExampleUserMigration extends User67Migration {
  function __construct() {
    // Description shown in the Migrate UI.
    $this->description = t('Migrate example users');

    // Set up the mapping of role ID numbers on the Drupal 6 site to role ID
    // numbers on the Drupal 7 site.
    $this->roleMap = array(
      // Anonymous.
      1 => 1,
      // Authenticated.
      2 => 2,
      // Full admin.
      3 => 5,
      // Other roles would go here.
    );

    // Set up the input/text format mapping array of ID integer on the Drupal 6
    // site to machine name on the Drupal 7 site.
    $this->formatMap = array();
    $this->formatMap[0] = 'plain_text';
    $this->formatMap[1] = 'filtered_html';
    $this->formatMap[2] = 'full_html';

    // Call the parent constructor, and you're done.
    parent::__construct();
  }
}
