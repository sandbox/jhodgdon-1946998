<?php

/**
 * @file
 * Migrates a sample taxonomy from Drupal 6 to 7.
 */

/**
 * Migrates terms from an example taxonomy vocabulary from Drupal 6 to 7.
 *
 * You must have already set up the vocabulary on the Drupal 7 site.
 */
class ExampleTermsImageTypeMigration extends Taxonomy67Migration {
  function __construct() {

    // Description shown in the Migrate UI.
    $this->description = t('Migrate example Image Type taxonomy');

    // Vocabulary ID in the Drupal 6 site.
    $this->sourceVid = 2;

    // Vocabulary machine name in the Drupal 7 site.
    $this->targetVocabularyMachineName = 'image_type';

    parent::__construct();
  }
}
