<?php

/**
 * @file
 * Migrates a sample content type from Drupal 6 to 7.
 */

/**
 * Migrates nodes from an example content type from Drupal 6 to 7.
 *
 * You must have already set up the content type and its fields on the Drupal
 * 7 site. You'll need to modify the constructor here to match your own site's
 * content type set-up.
 */
class ExampleContentImageMigration extends Content67Migration {
  function __construct() {

    // Description shown in the Migrate UI.
    $this->description = t('Migrate example Image content');

    // Set up the input/text format mapping array of ID integer on Drupal 6
    // site to machine name on Drupal 7 site.
    $this->formatMap = array();
    $this->formatMap[0] = 'plain_text';
    $this->formatMap[1] = 'filtered_html';
    $this->formatMap[2] = 'full_html';

    // Define the content type machine names on Drupal 6 and 7 sites.
    $this->sourceContentType = 'image_for_db';
    $this->targetContentType = 'image_for_db';

    // Assume one of the fields has changed names between Drupal 6 and 7.
    // Mapping is Drupal 7 name => Drupal 6 name.
    // Do not include taxonomy fields, or fields whose names did not change.
    $this->fieldMap = array(
      'field_img_long_description' => 'field_img_long_desc',
    );

    // Define which taxonomy vocabularies are used. Mapping is field name on
    // Drupal 7 site => vocabulary ID on Drupal 6 site.
    $this->sourceVids = array(
      'field_image_type' => 2,
    );

    // Define which migrations to reference for users and taxonomies.
    // Aside from the special key 'user', mapping is field name on Drupal 7
    // site => migration machine name.
    $this->referenceMigrations = array(
      'user' => 'ExampleUser',
      'field_image_type' => 'ExampleTermsImageType',
    );

    parent::__construct();
  }
}
