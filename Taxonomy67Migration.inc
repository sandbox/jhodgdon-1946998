<?php

/**
 * @file
 * Contains class Taxonomy67Migration, for migrating taxonomy.
 *
 * Code here is adapted from drupal.org (Migrate module documentation, Migrate
 * sample modules, and the "xdeb" sandbox example project at
 * http://drupal.org/sandbox/frjo/1332996).
 *
 * Adaptation by Jennifer Hodgdon of Poplar ProductivityWare.
 */

/**
 * Defines a base class for taxonomy migration from Drupal 6 to 7.
 *
 * To use this class for a migration, for each vocabulary you want to migrate:
 * - Set up the vocabulary in the target (Drupal 7) site.
 * - Extend this class in your own class.
 * - In your class's constructor (see documentation for each member variable):
 *   - Set $this->sourceVid
 *   - Set $this->targetVocabularyMachineName
 *   - Set $this->description (from Migrate::$description)
 *   - Call parent::__construct()
 * - Put your class into your implementation of hook_migrate_api().
 *
 * Some notes on the limitations of this class and how to get it to work:
 * - Term languages are not supported. This is because the Migrate
 *   module does not support importing individual languages for each term. You
 *   will need to figure out some other way to migrate your translations of
 *   terms if you have defined them.
 * - If you have the Pathauto module, disable it during user migration, if
 *   you want to keep the path aliases from your Drupal 6 site.
 * - This class depends on a couple of PHP constants that are set up in
 *   migrate67.module (which also has a settings form for them).
 */
class Taxonomy67Migration extends Migration {

  /**
   * Source (Drupal 6) vocabulary ID.
   *
   * @var int
   */
  protected $sourceVid;

  /**
   * Destination (Drupal 7) vocabulary machine name.
   *
   * @var string
   */
  protected $targetVocabularyMachineName;

  /**
   * Sets up the source, destination, and mapping for migration.
   *
   * Extending classes need to set up:
   * - $this->sourceVid
   * - $this->targetVocabularyMachineName
   * - $this->description
   * before calling this (parent) constructor.
   */
  public function __construct() {
    parent::__construct();

    // Set up source query to read taxonomy terms for a single vocabulary
    // from a D6 database.
    $source_fields = array(
      'tid' => t('Term ID'),
      'url_alias' => t('URL alias'),
    );

    $query = db_select(MIGRATE67_DATABASE_PREFIX . 'term_data', 'td')
      ->fields('td', array('tid', 'vid', 'name', 'description', 'weight'))
      ->condition('td.vid', $this->sourceVid);
    $query->join(MIGRATE67_DATABASE_PREFIX . 'term_hierarchy', 'th', 'td.tid = th.tid');
    $query->addField('th', 'parent');
    $query->orderBy('th.parent', 'ASC');

    $this->source = new MigrateSourceSQL($query, $source_fields);

    // Set up destination as a taxonomy term in the target vocabulary.
    $this->destination = new MigrateDestinationTerm($this->targetVocabularyMachineName);

    // Set up mapping.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'tid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'D6 Term ID',
          'alias' => 'td',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // Make the individual field mappings.
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('description', 'description');
    $this->addFieldMapping('weight', 'weight');
    $this->addFieldMapping('parent', 'parent')->sourceMigration($this->getMachineName());
    $this->addFieldMapping('path', 'url_alias');

    // Unmapped source fields
    $this->addUnmigratedSources(array('vid'));

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array('parent_name', 'format'));
  }

  /**
   * Prepares a taxonomy term for saving: URL alias and parent fields.
   *
   * If you override this method and in your class, return FALSE for a
   * particular row, the term will not be saved.
   */
  public function prepareRow($current_row) {
    if ($current_row->parent == 0) {
      unset($current_row->parent);
    }

    $current_row->url_alias = $this->getUrlAlias($current_row->tid);

    return TRUE;
  }

  /**
   * Finds the URL alias for a taxonomy field.
   */
  protected function getUrlAlias($tid) {
    $result = db_select(MIGRATE67_DATABASE_PREFIX . 'url_alias', 'ua')
      ->fields('ua', array('dst'))
      ->condition('ua.src', 'taxonomy/term/' . $tid)
      ->execute()
      ->fetchObject();

    return is_object($result) && !empty($result->dst) ? $result->dst : NULL;
  }
}
