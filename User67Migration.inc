<?php

/**
 * @file
 * Contains class User67Migration, for migrating users.
 *
 * Code here is adapted from drupal.org (Migrate module documentation, Migrate
 * sample modules, and the "xdeb" sandbox example project at
 * http://drupal.org/sandbox/frjo/1332996).
 *
 * Adaptation by Jennifer Hodgdon of Poplar ProductivityWare.
 */

/**
 * Defines a base class for user migration from Drupal 6 to 7.
 *
 * To use this class for a migration:
 * - Extend this class in your own class.
 * - In your class's constructor (see documentation for each member variable):
 *   - Set up $this->roleMap
 *   - Set $this->formatMap
 *   - Set $this->description (from Migrate::$description)
 *   - Set $this->onlyActive and $this->onlyWithRoles if desired
 *   - Call parent::__construct()
 * - Put your class into your implementation of hook_migrate_api() and in your
 *   module's .inf file.
 *
 * Some notes on the limitations of this class and how to get it to work:
 * - User profile fields are not migrated.
 * - User pictures are not migrated.
 * - User theme selection is not migrated.
 * - If you have the Pathauto module, disable it during user migration, if
 *   you want to keep the path aliases from your Drupal 6 site. This module will
 *   migrate the Drupal 6 site's aliases.
 * - This class depends on a couple of PHP constants that are set up in
 *   migrate67.module (which also has a settings form for them).
 */
class User67Migration extends Migration {

  /**
   * Map of source (D6) => destination (D7) role IDs.
   *
   * @var array
   */
  protected $roleMap = array();

  /**
   * Array mapping text formats: source numeric ID => destination machine name.
   *
   * You only need this for formats used in user signatures.
   *
   * @var array
   */
  protected $formatMap = array();

  /**
   * TRUE to import only active users.
   *
   * Override this if you want to import only active users. Otherwise, all users
   * will be imported.
   *
   * @var bool
   */
  protected $onlyActive = FALSE;

  /**
   * TRUE to import only users who have been assigned specific roles.
   *
   * Override this if you want to import only users who have been assigned
   * roles (other than the "authenticated user" role).
   *
   * @var bool
   */
  protected $onlyWithRoles = FALSE;

  /**
   * Sets up the description, source, destination, and mapping for migration.
   *
   * Extending class constructors should call this one after setting up
   * $roleMap and $formatMap.
   */
  public function __construct() {
    parent::__construct();

    // Set up source query to read users from Drupal 6 table.
    $source_fields = array(
      'uid' => t('User ID'),
      'roles' => t('The set of roles assigned to a user.'),
      'url_alias' => t('URL alias'),
    );

    $query = db_select(MIGRATE67_DATABASE_PREFIX . 'users', 'u')
      ->fields('u', array('uid', 'name', 'pass', 'mail', 'created', 'access', 'login', 'status', 'init', 'signature', 'signature_format', 'language', 'timezone_name'))
      ->condition('u.uid', 1, '>');
    if ($this->onlyActive) {
      $query->condition('u.status', 1);
    }
    if ($this->onlyWithRoles) {
      $query->distinct(TRUE);
      $query->innerJoin(MIGRATE67_DATABASE_PREFIX . 'users_roles', 'r', 'u.uid = r.uid');
    }

    $query->orderBy('u.uid', 'ASC');

    $this->source = new MigrateSourceSQL($query, $source_fields);

    // Set up destination as new users.
    $this->destination = new MigrateDestinationUser(array('md5_passwords' => TRUE));

    // Set up mapping from source to destination.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'uid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'User ID',
          'alias' => 'u',
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );

    // Make the individual field mappings.
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('name', 'name')->dedupe('users', 'name');
    $this->addFieldMapping('pass', 'pass');
    $this->addFieldMapping('mail', 'mail')->dedupe('users', 'mail');
    $this->addFieldMapping('language', 'language');
    $this->addFieldMapping('signature', 'signature');
    $this->addFieldMapping('signature_format', 'signature_format');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('access', 'access');
    $this->addFieldMapping('login', 'login');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('init', 'init');
    $this->addFieldMapping('timezone', 'timezone_name');
    $this->addFieldMapping('roles', 'roles');
    $this->addFieldMapping('path', 'url_alias');
    $this->addUnmigratedDestinations(array('role_names', 'data', 'theme', 'picture'));
  }

  /**
   * Prepares a user object for saving: adds roles and URL alias.
   *
   * If you override this method and in your class, return FALSE for a
   * particular row, the user will not be saved.
   */
  public function prepareRow($current_row) {
    $source_id = $current_row->uid;

    // Find the text format for the node.
    $current_row->signature_format = isset($this->formatMap[$current_row->signature_format]) ? $this->formatMap[$current_row->signature_format] : $this->formatMap[0];

    // Read the roles in from the D6 database and map to D7 roles.
    $results = db_select(MIGRATE67_DATABASE_PREFIX . 'users_roles', 'r')
      ->fields('r', array('uid', 'rid'))
      ->condition('r.uid', $source_id)
      ->execute();

    $roles = array();
    foreach ($results as $row) {
      $roles[] = $this->roleMap[$row->rid];
    }

    $current_row->roles = $roles;

    $current_row->url_alias = $this->getUrlAlias($current_row->uid);

    return TRUE;
  }

  /**
   * Finds the existing URL alias for a user in the source (D6) site.
   */
  protected function getUrlAlias($uid) {
    $result = db_select(MIGRATE67_DATABASE_PREFIX . 'url_alias', 'ua')
      ->fields('ua', array('dst'))
      ->condition('ua.src', 'user/' . $uid)
      ->execute()
      ->fetchObject();

    return is_object($result) && !empty($result->dst) ? $result->dst : NULL;
  }
}
