Note: This is a Sandbox project. You should probably check out
https://drupal.org/project/migrate_d2d instead.

----------

This module provides a basic structure for migrating users, taxonomy, and
content from a Drupal 6 site to a Drupal 7 site. The classes and code are not
optimized for speed, but have the advantage of actually working, with a
minimal effort on your part.

To use this module, you will need to do all of the following steps:

1. Set up a Drupal 7 site with taxonomy vocabularies and content types (with
   fields, languages, etc. all set up), on the same server as your Drupal 6
   site.

2. Enable this module in the Drupal 7 site. It depends on Migrate
   (http://drupal.org/project/migrate).

3. Visit the "6/7 Migration Settings" page (a sub-page of the Migrate UI) at
   path admin/content/import/migrate67 on your Drupal 7 site, enter the
   information on where to find the Drupal 6 site and database, and save the
   settings.

4. Create your own module for migration (the "example" subdirectory provides a
   good starting point). In this module, you will need to:

   a. Create classes to migrate users, each taxonomy vocabulary, and each
      content type. Each class you create will extend one of the base classes in
      this module (such as Migrate67User, etc.). There are notes in each base
      class explaining what you need to do -- even if you use the "example"
      starting point, you still really ought to read the notes in the base
      classes too. Don't worry, it's just a few lines of code for each class.

   b. Put each of your example classes in a ".inc" file, and put each .inc
      file into your module's .info file as a files[] line (see example module).

   c. Declare each migration class in your module's implementation of
      hook_migrate_api(). Again, check the example module to find out how.

5. Enable your module (in the Drupal 7 site).

6. Visit the Migrate UI page (admin/content/migrate) and migrate your data, or
   use Drush to migrate your data. However, if you plan to use Drush, be sure
   read the note in the Migrate67Content base class about Drush if you are
   migrating content with attached files or images.

7. Once you are done with migrating, you can disable all migration-related
   modules.
